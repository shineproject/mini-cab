﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using MINI_Cab.Models;
using System.Net;
using System.Net.Mail;
using MINI_Cab.Areas.Admin.Models;
using System.Xml;

namespace MINI_Cab.App_Code
{
    public class Common_Function
    {
        #region minicab
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public string CheckLogin(string usm_fname, string usm_password)
        {
            string UserRole = "0";
            string UserId = "0";
            string UserName = "";
            string UserImg = "";
            try
            {
                ConnectionCheck();
                SqlCommand command = new SqlCommand("CheckLogin_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_email", usm_fname);
                command.Parameters.AddWithValue("@usm_password", usm_password);
                SqlDataReader rdr = command.ExecuteReader();
                if (rdr.Read())
                {
                    UserId = rdr["usm_id"].ToString();
                    UserRole = rdr["usm_userrole"].ToString();
                    UserName = rdr["usm_fname"].ToString() + " " + rdr["usm_lname"].ToString();
                    UserImg = rdr["usm_img"].ToString();
                }
                rdr.Close();
                con.Close();
                return UserId + "," + UserRole + "," + UserName + "," + UserImg;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int AddUserMaster(UserManagement z)
        {
            try
            {
                int Action = 0;
                int UserMasterid = 0;
                SqlCommand command;
                ConnectionCheck();
                if (z.usm_id == 0)
                {
                    command = new SqlCommand("Add_UserManage_SP", con);
                    Action = 1;
                }
                else
                {
                    command = new SqlCommand("Add_UserManage_SP", con);
                    command.Parameters.AddWithValue("@usm_id", z.usm_id);
                    Action = 2;
                }

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@usm_fname", z.usm_fname);
                command.Parameters.AddWithValue("@usm_lname", z.usm_lname);
                command.Parameters.AddWithValue("@usm_password", z.usm_password);
                command.Parameters.AddWithValue("@usm_email", z.usm_email);
                command.Parameters.AddWithValue("@usm_phone", z.usm_phone);
                command.Parameters.AddWithValue("@usm_address", z.usm_address);
                command.Parameters.AddWithValue("@usm_city", z.usm_city);
                command.Parameters.AddWithValue("@usm_state", z.usm_state);
                command.Parameters.AddWithValue("@usm_country", z.usm_country);
                command.Parameters.AddWithValue("@usm_userrole", z.usm_userrole);
                command.Parameters.AddWithValue("@usm_com_id", z.usm_com_id);
                command.Parameters.AddWithValue("@usm_brn_id", z.usm_brn_id);
                command.Parameters.AddWithValue("@usm_status", "0");
                command.Parameters.AddWithValue("@usm_createddate", DateTime.Now);
                command.Parameters.AddWithValue("@usm_createdby", z.usm_createdby);
                command.Parameters.AddWithValue("@usm_updateddate", DateTime.Now);
                command.Parameters.AddWithValue("@usm_updatedby", z.usm_updatedby);
                command.Parameters.AddWithValue("@Action", Action);

                if (Action == 2)
                {
                    command.ExecuteNonQuery();
                    UserMasterid = z.usm_id;
                }
                else
                {
                    UserMasterid = (int)command.ExecuteScalar();
                }
                con.Close();
                return UserMasterid;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public string CheckUsername(string Email)
        {
            try
            {
                string status_msg = "";
                SqlCommand command;
                ConnectionCheck();
                command = new SqlCommand("ChangePassword_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_email", Email);
                command.Parameters.AddWithValue("@Action", "CheckUsername");
                int result = (int)command.ExecuteScalar();
                if (result != 0)
                {
                    status_msg = "yes";
                }
                else { status_msg = "no"; }
                con.Close();
                return status_msg;
            }
            catch (Exception)
            {

                throw;
            }


        }
        public string RecoverPassword(string email)
        {
            try
            {
                string _Email = "";
                string _Password = "";
                string _UserId = "";
                ConnectionCheck();
                if (email != null)
                {
                    SqlCommand command = new SqlCommand("RecoverPassword_Sp", con);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@usm_email", email);
                    SqlDataReader rdr = command.ExecuteReader();
                    if (rdr.Read())
                    {
                        _UserId = rdr["usm_id"].ToString();
                        _Email = rdr["usm_email"].ToString();
                        _Password = rdr["usm_password"].ToString();
                    }
                }
                con.Close();
                return _Email + "," + _Password + "," + _UserId;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet GetUserData(int id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetUserProfile_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@userId", id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public string ChangePassword(string OldPasword, string NewPassword, string UserId)
        {
            try
            {
                string status_msg = "";
                SqlCommand command;
                ConnectionCheck();

                command = new SqlCommand("ChangePassword_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_id", UserId);
                command.Parameters.AddWithValue("@Old_Password", OldPasword);
                command.Parameters.AddWithValue("@New_Password", NewPassword);
                command.Parameters.AddWithValue("@Action", "Check");
                int result = (int)command.ExecuteScalar();
                if (result != 0)
                {
                    status_msg = Confirm_ChangePassword(UserId, OldPasword, NewPassword);
                }
                else { status_msg = "OLD Password Not Match"; }
                con.Close();
                return status_msg;
            }
            catch (Exception)
            {

                throw;
            }


        }
        public string Confirm_ChangePassword(string UserId, string OldPasword, string NewPassword)
        {
            try
            {
                SqlCommand command;
                ConnectionCheck();
                command = new SqlCommand("ChangePassword_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_id", UserId);
                command.Parameters.AddWithValue("@Old_Password", OldPasword);
                command.Parameters.AddWithValue("@New_Password", NewPassword);
                command.Parameters.AddWithValue("@Action", "UpdatePassword");
                command.ExecuteNonQuery();
                con.Close();
                return "Change Password Successfully";
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet LastFiveJobs(int userId)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("LastFiveJobs_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@User_ID", userId);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet track_a_cab(int user_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("track_a_cab_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@user_id", user_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void UpdateProfile(string usm_id, string usm_img)
        {
            try
            {
                SqlCommand command;
                ConnectionCheck();
                command = new SqlCommand("UploadProfile_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_id", usm_id);
                command.Parameters.AddWithValue("@usm_img", usm_img);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }

        //bilal changies
        public DataSet get_tariffs(int tariff_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("get_tariff_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tariff_id", tariff_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public DataSet getDriverTarrifwise(int driver_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("getDriverTarrifwise_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@JobId", driver_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet get_drivers(int driver_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("get_driver_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@driver_id", driver_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public string ADD_tariffs(tariff_model t)
        {
            var result = "fail";
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("add_tariff_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tariff_id", t.tariff_id);
                command.Parameters.AddWithValue("@tariff_name", t.tariff_name);
                command.Parameters.AddWithValue("@tariff_initial_charge", t.tariff_initial_charge);
                command.Parameters.AddWithValue("@tariff_include_miles", t.tariff_include_miles);
                command.Parameters.AddWithValue("@tariff_per_mile", t.tariff_per_mile);
                command.Parameters.AddWithValue("@tariff_colour", t.tariff_colour.ToUpper());
                command.Parameters.AddWithValue("@createdate", t.createdate);
                command.Parameters.AddWithValue("@tariff_location", t.tariff_location);
                command.Parameters.AddWithValue("@updateddate", t.updateddate);
                command.Parameters.AddWithValue("@created_by", t.created_by);
                command.Parameters.AddWithValue("@updated_by", t.updated_by);
                command.Parameters.AddWithValue("@status", "0");
               // command.ExecuteNonQuery();
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected == 1)
                {
                    result = "success";
                }
                    con.Close();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        public void ADD_OverRide(string or_Id,string Reason,DateTime date,int Driver_no,int userId)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("Driver_OverRide_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@D_OR_Id", or_Id);
                command.Parameters.AddWithValue("@D_OR_Reason", Reason);
                command.Parameters.AddWithValue("@D_OR_Date", date);
                command.Parameters.AddWithValue("@D_OR_Uni_No", Driver_no);
                command.Parameters.AddWithValue("@D_OR_CreatedBy", userId);
                command.Parameters.AddWithValue("@D_OR_CreatedDate", DateTime.Now);
                command.Parameters.AddWithValue("@D_OR_UpdatedDate", DateTime.Now);
                command.Parameters.AddWithValue("@D_OR_UpdatedBy", userId);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }

        }
        public void ADD_PaymentMethod(Payment P)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("Payment_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Pay_Name", P.Pay_Name);
                command.Parameters.AddWithValue("@Pay_Type", P.Pay_Type);
                command.Parameters.AddWithValue("@Pay_Status", 0);
                command.Parameters.AddWithValue("@Pay_CreatedDate", P.Pay_CreatedDate);
                command.Parameters.AddWithValue("@Pay_CreatedBy", P.Pay_CreatedBy);
                command.Parameters.AddWithValue("@Pay_UpdatedDate", P.Pay_UpdatedDate);
                command.Parameters.AddWithValue("@Pay_UpdatedBy", P.Pay_UpdatedBy);
                if (P.Pay_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", "1");
                }
                else
                {
                    command.Parameters.AddWithValue("@Pay_iD", P.Pay_Id);
                    command.Parameters.AddWithValue("@Action", "2");
                }
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public string ADD_drivers(driver_models d)
        {
            var result = "fail";
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("add_driver_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@driver_id", d.driver_id);
                command.Parameters.AddWithValue("@driver_name", d.driver_name);
                command.Parameters.AddWithValue("@driver_number", d.driver_number);
                command.Parameters.AddWithValue("@driver_email", d.driver_email);
                command.Parameters.AddWithValue("@driver_vehicle_reg", d.driver_vehicle_reg);
                command.Parameters.AddWithValue("@driver_vehicle_colour", d.driver_vehicle_colour);
                command.Parameters.AddWithValue("@drvier_vehicle_model", d.drvier_vehicle_model);
                command.Parameters.AddWithValue("@driver_tariff_id", d.driver_tariff_id);
                command.Parameters.AddWithValue("@driver_let", d.driver_let);
                command.Parameters.AddWithValue("@driver_lon", d.driver_lon);
                command.Parameters.AddWithValue("@driver_status", 0);
                command.Parameters.AddWithValue("@driver_uni_no", d.driver_uni_no);
                command.Parameters.AddWithValue("@driver_password", d.driver_password);
                command.Parameters.AddWithValue("@driver_pco_number", d.driver_pco_number);
                command.Parameters.AddWithValue("@driver_pco_expiry",d.driver_pco_expiry);
                command.Parameters.AddWithValue("@driver_phv_number", d.driver_phv_number);
                command.Parameters.AddWithValue("@driver_phv_expiry", d.driver_phv_expiry);
                command.Parameters.AddWithValue("@driver_mot_expiry",d.driver_mot_expiry);
                command.Parameters.AddWithValue("@driver_surname", d.driver_surname);
                command.Parameters.AddWithValue("@driver_insurance",d.driver_insurance);
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected == 1)
                {
                    result = "success";
                }
                con.Close();
            }
            catch (Exception ex)
            {
               // throw;
            }
            return result;
        }
        public void delete_tarif(int tariff_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("remove_tariff_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tariff_id", tariff_id);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void delete_driver(int driver_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("remove_driver_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@driver_id", driver_id);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void DeletePayment(int Pay_Id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("Payment_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Pay_Id", Pay_Id);
                command.Parameters.AddWithValue("@Action", "4");
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }

        }
        public int Add_from_job(fromjob_booking f)
        {
            try
            {
                SqlCommand command1;
                ConnectionCheck();
                command1 = new SqlCommand("add_fromjob_sp", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@from_job_name", f.from_job_name);
                command1.Parameters.AddWithValue("@from_job_number", f.from_job_number);
                command1.Parameters.AddWithValue("@from_job_street", f.from_job_street);
                command1.Parameters.AddWithValue("@from_job_postcode", f.from_job_postcode);
                command1.Parameters.AddWithValue("@from_job_address", f.from_job_address);
                command1.Parameters.AddWithValue("@from_job_notes", f.from_job_notes);
                command1.Parameters.AddWithValue("@from_job_time", f.from_job_time);
                command1.Parameters.AddWithValue("@VehicleType", f.vehicletype);
                command1.Parameters.AddWithValue("@status", f.status);
                command1.Parameters.AddWithValue("@user_id", f.user_id);
                command1.Parameters.AddWithValue("@Pay_data", f.PaymentData);
                command1.Parameters.AddWithValue("@Pay_TypeId", f.PaymentType);
                command1.Parameters.AddWithValue("@Amount", f.Amount);
                command1.Parameters.AddWithValue("@Miles", f.Miles);
                command1.Parameters.AddWithValue("@PickupTime", DateTime.Now);
                command1.Parameters.AddWithValue("@DropTime", DateTime.Now);
                command1.Parameters.AddWithValue("@WayType", f.WayType);
                int from_jobid = (int)command1.ExecuteScalar();
                con.Close();
                return from_jobid;
            }
            catch (Exception)
            {

                throw;
            }

        }
        
        public void Add_to_job(string tojob_street, string tojob_postcode, string tojob_address, int from_jobid)
        {
            try
            {
                SqlCommand command1;
                ConnectionCheck();
                command1 = new SqlCommand("add_tojob_SP", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@tojob_street", tojob_street);
                command1.Parameters.AddWithValue("@tojob_postcode", tojob_postcode);
                command1.Parameters.AddWithValue("@tojob_address", tojob_address);
                command1.Parameters.AddWithValue("@from_job_id", from_jobid);
                command1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet GetJobDateWise(string date)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetJOBDateWise_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@selectDate", date);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet GetPayment(int Pay_Id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("Payment_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                if (Pay_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", "3");
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", "5");
                    command.Parameters.AddWithValue("@Pay_Id", Pay_Id);
                }
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void assign_driver(int from_jobid, int drive_id, int user_id)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("assign_driver_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@job_id", from_jobid);
                command.Parameters.AddWithValue("@driver_id", drive_id);
                command.Parameters.AddWithValue("@user_id", user_id);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet GetAssignJobDateWise(string date)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetAssignJOBDateWise_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@selectDate", date);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet Get_today_jobdata(string date)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("get_today_job_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@selectDate", date);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet Get_DriverCount()
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("Driver_Count_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet Get_Activity(int from_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("Get_Activity_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("from_job_id", from_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string CheckDriverAvailable(string DriverId)
        {
            ConnectionCheck();
            SqlCommand command = new SqlCommand("DriverAvailable_SP", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@driverId", DriverId);

            int DriverStatus =(int)command.ExecuteScalar();
            con.Close();
            return DriverStatus.ToString();
        }

    }
    #endregion

    /// <summary>
    /// Email Send
    /// </summary>
    #region SendEmail
    public class SendEmail
    {
        protected string ServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
        protected string ServerUserName = ConfigurationManager.AppSettings["ServerUserName"].ToString();
        protected string ServerUserPassword = ConfigurationManager.AppSettings["ServerUserPassword"].ToString();
        protected string Port = ConfigurationManager.AppSettings["Port"].ToString();

        public string ReadFile(string filename)
        {
            try
            {

                System.IO.StreamReader oRead;
                string ApplicationPath = HttpContext.Current.Server.MapPath(filename);
                oRead = System.IO.File.OpenText(ApplicationPath);
                string MessageString = oRead.ReadToEnd();
                oRead.Close();
                oRead = null;
                return MessageString;
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }
        public string Email(string Email, string Password, string subject)
        {
            string status = "";
            try
            {
                object MessageString = null;
                if (subject == "Forgot Password")
                {
                    MessageString = ReadFile(ConfigurationManager.AppSettings["EmailPath"] + "\\" + "ForgetPassword.html");
                    MessageString = MessageString.ToString().Replace("{Email}", Email);
                    MessageString = MessageString.ToString().Replace("{Password}", Password);
                }
                else if (subject == "Verification Code")
                {
                    MessageString = ReadFile(ConfigurationManager.AppSettings["EmailPath"] + "\\" + "OTP.html");
                    MessageString = MessageString.ToString().Replace("{OTP}", Password);
                }

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(ServerUserName, Email);
                SmtpClient client = new SmtpClient();
                client.Port = Convert.ToInt32(Port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = ServerName;
                mail.Subject = subject;
                mail.Body = MessageString.ToString();
                mail.IsBodyHtml = true;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(ServerUserName, ServerUserPassword);
                client.Send(mail);
                status = "ok";
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                status = ex.Message.ToString();
            }
            return status;
        }
    }
    #endregion


}




