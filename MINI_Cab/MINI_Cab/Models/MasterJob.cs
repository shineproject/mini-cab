﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Models
{
    public class MasterJob
    {
        public string MasterJob_Id { get; set; }
        public string User_Id { get; set; }
        public string FromJob_Id { get; set; }
        public string ToJob_Id { get; set; }
        public int Status { get; set; }
    }
}