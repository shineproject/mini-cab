﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Models
{
    public class UserManagement
    {
        public int usm_id { get; set; }
        public string usm_fname { get; set; }
        public string usm_lname { get; set; }
        public string usm_password { get; set; }
        public string usm_email { get; set; }
        public string usm_phone { get; set; }
        public string usm_address { get; set; }
        public string usm_city { get; set; }
        public string usm_state { get; set; }
        public string usm_country { get; set; }
        public int usm_userrole { get; set; }
        public int usm_com_id { get; set; }
        public int usm_brn_id { get; set; }
        public string usm_status { get; set; }
        public DateTime usm_createddate { get; set; }
        public string usm_createdby { get; set; }
        public DateTime usm_updateddate { get; set; }
        public int usm_updatedby { get; set; }
    }
}