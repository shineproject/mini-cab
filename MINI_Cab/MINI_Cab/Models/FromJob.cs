﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Models
{
    public class FromJob
    {
        public string From_Job_Id { get; set; }
        public string From_Job_Name { get; set; }
        public string From_Job_Number { get; set; }
        public string From_Job_CurLoc { get; set; }
        public string From_Job_Email { get; set; }
        public string From_Job_Street { get; set; }
        public string From_Job_PostCode { get; set; }
        public string From_Job_Address { get; set; }
        public string From_Job_Notes { get; set; }
        public DateTime From_Job_Date { get; set; }
        public DateTime From_Job_Time { get; set; }
        public string From_Job_Recurring { get; set; }
        public string From_Job_Collection { get; set; }
        public int Vehicle_Type { get; set; }
    }
}