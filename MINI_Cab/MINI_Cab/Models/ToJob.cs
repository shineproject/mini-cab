﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Models
{
    public class ToJob
    {
        public string ToJob_Id { get; set; }
        public string ToJob_Name { get; set; }
        public string ToJob_Number { get; set; }
        public string ToJob_Street { get; set; }
        public string ToJob_PostCode { get; set; }
        public string ToJob_Address { get; set; }
        public string ToJob_Notes { get; set; }
    }
}