﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MINI_Cab;
using MINI_Cab.App_Code;
using Elmah;
using MINI_Cab.Models;

namespace MINI_Cab.Controllers
{
    public class LoginController : Controller
    {
        Common_Function db = new Common_Function();
        // GET: Login
        #region getMethod
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Registration()
        {
            return View();
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
        #endregion

        #region Method Post
        [HttpPost]
        public ActionResult CheckLogin(string Username, string Password)
        {
            string LoginDetails = "";
            string UserRole = "";
            string UserName = "";
            string UserImg = "";
            try
            {
                LoginDetails = db.CheckLogin(Username, Password);
                string[] values = LoginDetails.Split(',');
                string UserId = values[0];
                UserRole = values[1];
                UserName = values[2];
                UserImg = values[3];
                if (UserId != "0" && UserRole != "0")
                {
                    Session["UserID"] = UserId;
                    Session["UserName"] = UserName;
                    Session["UserImg"] = UserImg;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { RoleId = UserRole });
        }

        [HttpPost]
        public ActionResult RecoverPassword(string Email)
        {
            SendEmail objEmail = new SendEmail();
            string status = "";
            string email = "";
            string password = "";
            try
            {
                string _status = db.RecoverPassword(Email);
                string[] values = _status.Split(',');
                string UserId = values[2];
                if (UserId != "0" && UserId != "")
                {
                    email = values[0];
                    password = values[1];
                    string statusEmail = objEmail.Email(email, password, "Forgot Password");
                    if (statusEmail == "ok")
                    {
                        status = "success";
                    }
                    else { status = "send fails"; }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { status = status });
        }

        [HttpPost]
        public ActionResult AddReg(UserManagement objusermaster)
        {
            string msg = "ok";
            try
            {
                objusermaster.usm_createdby = "0";
                objusermaster.usm_updatedby = 0;
                int userid = db.AddUserMaster(objusermaster);
                Session["UserID"] = userid;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { message = msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckEmail(string Email)
        {
            string status = "";
            try
            {
                status = db.CheckUsername(Email);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { message = status }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendOTP(string Email, string OTP)
        {
            SendEmail objEmail = new SendEmail();
            string status = "";
            try
            {
                string statusEmail = objEmail.Email(Email, OTP, "Verification Code");
                //string statusEmail = "ok";
                if (statusEmail == "ok")
                {
                    status = "success";
                }
                else { status = "send fails"; }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { status = status });
        }
        #endregion
    }
}