﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Areas.Admin.Models
{
    public class tariff_model
    {
        public Int32 tariff_id { get; set; }
        public string tariff_name { get; set; }
        public decimal tariff_initial_charge { get; set; }
        public decimal tariff_include_miles { get; set; }
        public decimal tariff_per_mile { get; set; }
        public string tariff_colour { get; set; }
        public string tariff_location { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updateddate { get; set; }
        public int created_by { get; set; }
        public int updated_by { get; set; }
        public int status { get; set; }
    }
}