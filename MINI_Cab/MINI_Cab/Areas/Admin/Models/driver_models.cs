﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Areas.Admin.Models
{
    public class driver_models
    {
        public int driver_id { get; set; }
        public string driver_name { get; set; }
        public string driver_number { get; set; }
        public string driver_email { get; set; }
        public string driver_vehicle_reg { get; set; }
        public string driver_vehicle_colour { get; set; }
        public string drvier_vehicle_model { get; set; }
        public int driver_tariff_id { get; set; }
        public int driver_status { get; set; }
        public string driver_let { get; set; }
        public string driver_lon { get; set; }
        public string driver_password { get; set; }
        public string driver_uni_no { get; set; }
        public string driver_pco_number { get; set; }
        public string driver_pco_expiry { get; set; }
        public string driver_phv_number { get; set; }
        public string driver_phv_expiry { get; set; }
        public string driver_mot_expiry { get; set; }
        public string driver_surname { get; set; }
        public string driver_insurance { get; set; }
    }
}