﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Areas.Admin.Models
{
    public class tojob_booking
    {
        public int tojob_id { get; set; }
        public string tojob_street { get; set; }
        public string tojob_postcode { get; set; }
        public string tojob_notes { get; set; }
        public string tojob_address { get; set; }
    }
}