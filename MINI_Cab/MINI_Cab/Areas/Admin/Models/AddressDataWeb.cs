﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MINI_Cab.Areas.Admin.Models
{
    public class AddressDataWeb
    {
        public string AddressId { get; set; }
        public string AddressString { get; set; }
    }
}