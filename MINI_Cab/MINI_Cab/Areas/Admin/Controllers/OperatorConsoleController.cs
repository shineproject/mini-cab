﻿using Elmah;
using MINI_Cab.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MINI_Cab.Areas.Admin.Controllers
{
    public class OperatorConsoleController : Controller
    {
        Common_Function db = new Common_Function();
        // GET: Admin/OperatorConsole
        public ActionResult OperatorConsole()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        [HttpPost]
        public ActionResult assign_driver(int from_jobid, int drive_id)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    return RedirectToAction("Login", "Login", new { area = "" });
                }
                db.assign_driver(from_jobid, drive_id,Convert.ToInt32(Session["UserId"]));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = "success" });
        }
        [HttpPost]
        public ActionResult GetAssignJobsDateWise(DateTime Select_Chart_dt)
        {
            String dtNewAssign = Select_Chart_dt.ToString("yyyy-MM-dd");
            DataSet ds = db.GetAssignJobDateWise(dtNewAssign);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }
        [HttpPost]
        public ActionResult GetJobsDateWise(DateTime select_Date_JOB)
        {
            String dtNewJOB = select_Date_JOB.ToString("yyyy-MM-dd");
            DataSet ds = db.GetJobDateWise(dtNewJOB);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }
        [HttpPost]
        public ActionResult GetTodayJobs_data(DateTime Select_Date_get)
        {
            String dtNewAssign = Select_Date_get.ToString("yyyy-MM-dd");
            DataSet ds = db.Get_today_jobdata(dtNewAssign);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }
        public ActionResult Logout()
        {
            Session["UserId"] = null;
            Session["UserId"] = "";
            Session["UserImg"] = "";
            Session["UserImg"] = null;
            Session["UserID"] = "";
            Session["UserName"] = "";
            Session["UserID"] = null;
            Session["UserName"] = null;

            Session.Clear();

            return RedirectToAction("Login", "Login", new { area = "" });
        }
        [HttpPost]
        public ActionResult ActivityLog(int From_jobid)
        {
            DataSet ds = db.Get_Activity(From_jobid);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }
        [HttpPost]
        public ActionResult Find_Distance(string postcode1, string postcode2)
        {
            return Json(new { data = 0 });
        }
        [HttpPost]
        public ActionResult get_Driver_List()
        {
            DataSet ds_driver = new DataSet();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow_driver = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_driver;
            try
            {
                ds_driver = db.get_drivers(0);
                foreach (DataRow row in ds_driver.Tables[0].Rows)
                {
                    childRow_driver = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_driver.Tables[0].Columns)
                    {
                        childRow_driver.Add(col.ColumnName, row[col]);
                    }
                    parentRow_driver.Add(childRow_driver);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { data_driver = jsSerializer.Serialize(parentRow_driver) });
        }

        [HttpPost]
        public ActionResult DriverAvailable(string DriverId)
        {
            string driverStatus = "0";
            try
            {
                 driverStatus = db.CheckDriverAvailable(DriverId);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
          
            return Json(new { result = driverStatus });
        }
    }
}