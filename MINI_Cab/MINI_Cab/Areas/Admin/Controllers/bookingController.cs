﻿using Elmah;
using getAddress.Sdk;
using getAddress.Sdk.Api.Requests;
using getAddress.Sdk.Api.Responses;
using MINI_Cab.App_Code;
using MINI_Cab.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MINI_Cab.Areas.Admin.Controllers
{
    public class bookingController : Controller
    {
        // GET: Admin/booking
        Common_Function db = new Common_Function();
        public ActionResult booking_form()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        [HttpPost]
        public ActionResult book_a_job(fromjob_booking datafrom_job, string datato_job)
        {
            string msg = "";
            try
            {
                datafrom_job.user_id =Convert.ToInt32(Session["UserId"]);
                int from_jobid = db.Add_from_job(datafrom_job);

                string[] to_job = datato_job.TrimEnd('+').Split('+');
                foreach (var job in to_job)
                {
                    string[] single_job = job.Split('$');
                    db.Add_to_job(single_job[2], single_job[1], single_job[0], from_jobid);
                }
                msg = "sucess";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }
        [HttpPost]
        public async Task<ActionResult> get_postcode_to_address(string postcode)
        {
            var apiKey = new ApiKey(ConfigurationManager.AppSettings["AddressCheckAPIKey"]);
            JsonResponse resp = new JsonResponse();
            using (var api = new GetAddesssApi(apiKey))
            {
                var result = await api.Address.Get(new GetAddressRequest(postcode));

                if (result.IsSuccess)
                {
                    var successfulResult = (GetAddressResponse.Success)result;
                    //  int count = 0;
                    List<AddressDataWeb> addresses = new List<AddressDataWeb>();
                    foreach (var address in successfulResult.Addresses)
                    {
                        string newaddress = null;

                        if (address.Line1 != null && address.Line1 != " " && address.Line1 != "")
                        {
                            newaddress = address.Line1;
                        }
                        if (address.Line2 != null && address.Line2 != " " && address.Line2 != "")
                        {
                            newaddress = newaddress + ", " + address.Line2;
                        }
                        if (address.Line3 != null && address.Line3 != " " && address.Line3 != "")
                        {
                            newaddress = newaddress + ", " + address.Line3;
                        }
                        if (address.Line4 != null && address.Line4 != " " && address.Line4 != "")
                        {
                            newaddress = newaddress + ", " + address.Line4;
                        }
                        if (address.TownOrCity != null && address.TownOrCity != " " && address.TownOrCity != "")
                        {
                            newaddress = newaddress + ", " + address.TownOrCity;
                        }
                        if (address.Locality != null && address.Locality != " " && address.Locality != "")
                        {
                            newaddress = newaddress + ", " + address.Locality;
                        }
                        if (address.County != null && address.County != " " && address.County != "")
                        {
                            newaddress = newaddress + ", " + address.County;
                        }

                        addresses.Add(new AddressDataWeb { AddressId = newaddress, AddressString = newaddress });
                    }
                    resp.data = addresses;
                    resp.status = true;
                    resp.message = "Postcode is valid";
                    return Json(resp);
                }
                else
                {
                    resp.data = null;
                    resp.status = false;
                    resp.message = "Please enter valid postcode";
                    return Json(resp);
                }
            }

        }
       
    }
}
public class JsonResponse
{
    public JsonResponse()
    {
        status = false;
        message = "";
        id = 0;
        data = new object();
    }
    public Boolean status { get; set; }
    public string message { get; set; }
    public int id { get; set; }
    public object data { get; set; }
    public int HttpStatusCode { get; set; }
}