﻿using MINI_Cab.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MINI_Cab.Areas.Admin.Controllers
{
    public class Admin_DashboardController : Controller
    {
        // GET: Admin/Admin_Dashboard
        Common_Function db = new Common_Function();
        public ActionResult Admin_Dashboard()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        public ActionResult UHD_Admin_Dashboard()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        [HttpPost]
        public ActionResult Get_DriverCount()
        {
            DataSet ds = db.Get_DriverCount();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }
    }
}