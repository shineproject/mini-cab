﻿using MINI_Cab.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MINI_Cab.Areas.Admin.Models;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Elmah;

namespace MINI_Cab.Areas.Admin.Controllers
{
    public class AdminPannelController : Controller
    {
        Common_Function db = new Common_Function();

        string motdata = "";
        // GET: Admin/AdminPannel
        public ActionResult AdminPanel()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        [HttpPost]
        public ActionResult get_tarrifs()
        {
            DataSet ds_tariff = new DataSet();
            DataSet ds_driver = new DataSet();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow_tarif = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_tarif;
            List<Dictionary<string, object>> parentRow_driver = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_driver;
            try
            {
                ds_tariff = db.get_tariffs(0);
                foreach (DataRow row in ds_tariff.Tables[0].Rows)
                {
                    childRow_tarif = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_tariff.Tables[0].Columns)
                    {
                        childRow_tarif.Add(col.ColumnName, row[col]);
                    }
                    parentRow_tarif.Add(childRow_tarif);
                }
                ds_driver = db.get_drivers(0);
                foreach (DataRow row in ds_driver.Tables[0].Rows)
                {
                    childRow_driver = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_driver.Tables[0].Columns)
                    {
                        childRow_driver.Add(col.ColumnName, row[col]);
                    }
                    parentRow_driver.Add(childRow_driver);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { data_tarif = jsSerializer.Serialize(parentRow_tarif), data_driver = jsSerializer.Serialize(parentRow_driver) });
        }

        [HttpPost]
        public ActionResult Get_Payment(int Pay_Id)
        {
            DataSet ds_Payment = new DataSet();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow_Payment = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_Payment;
            try
            {
                ds_Payment = db.GetPayment(Pay_Id);
                foreach (DataRow row in ds_Payment.Tables[0].Rows)
                {
                    childRow_Payment = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_Payment.Tables[0].Columns)
                    {
                        childRow_Payment.Add(col.ColumnName, row[col]);
                    }
                    parentRow_Payment.Add(childRow_Payment);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { data_Payment = jsSerializer.Serialize(parentRow_Payment) });
        }

        [HttpPost]
        public ActionResult getDriverTarrifwise(int jobid)
        {
            DataSet ds_tariff = new DataSet();
            DataSet ds_driver = new DataSet();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow_tarif = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_tarif;
            List<Dictionary<string, object>> parentRow_driver = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow_driver;
            try
            {
                ds_tariff = db.get_tariffs(0);
                foreach (DataRow row in ds_tariff.Tables[0].Rows)
                {
                    childRow_tarif = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_tariff.Tables[0].Columns)
                    {
                        childRow_tarif.Add(col.ColumnName, row[col]);
                    }
                    parentRow_tarif.Add(childRow_tarif);
                }
                ds_driver = db.getDriverTarrifwise(jobid);
                foreach (DataRow row in ds_driver.Tables[0].Rows)
                {
                    childRow_driver = new Dictionary<string, object>();
                    foreach (DataColumn col in ds_driver.Tables[0].Columns)
                    {
                        childRow_driver.Add(col.ColumnName, row[col]);
                    }
                    parentRow_driver.Add(childRow_driver);
                }
            }
            catch (Exception ex)
            {

                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { data_tarif = jsSerializer.Serialize(parentRow_tarif), data_driver = jsSerializer.Serialize(parentRow_driver) });
        }
        [HttpPost]
        public ActionResult ADD_a_tariffs(tariff_model t)
        {
            string msg = "";
            try
            {
                t.createdate = DateTime.Now;
                t.created_by = 1;
                t.updateddate = DateTime.Now;
                t.updated_by = 1;
                string result = db.ADD_tariffs(t);
                if (result == "success")
                { msg = "success"; }
                else { msg = "fail"; }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult ADD_Payment(Payment P)
        {
            string msg = "";
            try
            {
                P.Pay_CreatedDate = DateTime.Now;
                P.Pay_CreatedBy = Convert.ToInt32(Session["UserId"]);
                P.Pay_UpdatedDate = DateTime.Now;
                P.Pay_UpdatedBy = Convert.ToInt32(Session["UserId"]);
                db.ADD_PaymentMethod(P);
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult ADD_a_drivers(driver_models d)
        {
            string msg = "";
            try
            {
                DateTime driver_insurance = DateTime.Parse(d.driver_insurance.ToString(), CultureInfo.CreateSpecificCulture("fr-FR"));
                //DateTime vehi_pco_expiry1 = DateTime.Parse(d.driver_phv_expiry, CultureInfo.CreateSpecificCulture("fr-FR"));
                //DateTime driver_insurance1 = DateTime.Parse(d.driver_insurance, CultureInfo.CreateSpecificCulture("fr-FR"));
                //////d.driver_pco_expiry = driver_pco_expiry1.ToString();
                //////d.driver_phv_expiry = vehi_pco_expiry1.ToString();
                //d.driver_insurance = driver_insurance1.ToString();
                d.driver_insurance = driver_insurance.ToString("MM/dd/yyyy");
                if (d.driver_insurance == null)
                {
                    d.driver_insurance = DateTime.MaxValue.ToString("MM/dd/yyyy");
                }

                string result = db.ADD_drivers(d);
                if (result == "success")
                { msg = "success"; }
                else { msg = "fail"; }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult delete_tariff(int tarif_id)
        {
            string msg = "";
            try
            {
                db.delete_tarif(tarif_id);
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult delete_driver(int driver_id)
        {
            string msg = "";
            try
            {
                db.delete_driver(driver_id);
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult DeletePayment(int Payment_Id)
        {
            string msg = "";
            try
            {
                db.DeletePayment(Payment_Id);
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }

        [HttpPost]
        public ActionResult OverRide(string or_Id, string reason, DateTime date, int driverNo)
        {
            string msg = "";
            try
            {
                db.ADD_OverRide(or_Id, reason, date, driverNo, Convert.ToInt32(Session["UserId"]));
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { msg = msg });
        }
        public async Task<string> CallApi(string Registration)
        {
            try
            {
                var client = new HttpClient();
                motdata = "";
                client.DefaultRequestHeaders.Add("x-api-key", "87hba6NVob2Yu3BdXzUnV11kj2O9NmU161IMxnAE");
                client.Timeout = TimeSpan.FromSeconds(30);
                motdata = await client.GetStringAsync("https://beta.check-mot.service.gov.uk/trade/vehicles/mot-tests?registration=" + Registration);
                if (motdata != null && motdata != "")
                {
                    return motdata;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> DVLApi(string Registration)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return Json(new { data = jsSerializer.Serialize(await CallApi(Registration)) });
        }
    }
}