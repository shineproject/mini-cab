﻿using Elmah;
using MINI_Cab.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MINI_Cab.Models;
using System.IO;
//using MINI_Cab.Areas.User.Models;

namespace MINI_Cab.Areas.User.Controllers
{
    public class User_DashboardController : Controller
    {
        Common_Function db = new Common_Function();
        // GET: User/User_Dashboard
        #region GET_Method
        public ActionResult Dashboard()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            DataSet ds = db.LastFiveJobs(Convert.ToInt32(Session["UserId"]));
            return View(ds);
        }
        public ActionResult Book_a_job()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        public ActionResult Logout()
        {
            Session["UserId"] = null;
            Session["UserId"] = "";
            Session["UserImg"] = "";
            Session["UserImg"] = null;
            Session["UserID"] = "";
            Session["UserName"] = "";
            Session["UserID"] = null;
            Session["UserName"] = null;

            Session.Clear();

            return RedirectToAction("Login", "Login", new { area = "" });
        }
        public ActionResult Track_a_cab()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        public ActionResult User_profile()
        {
            if (Session["UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            return View();
        }
        #endregion

        #region POST Method

        [HttpPost]
        public ActionResult getUserProfile()
        {
            DataSet ds = db.GetUserData(Convert.ToInt32(Session["UserId"]));
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            try
            {
                if (Session["UserId"] == null)
                {
                    return RedirectToAction("Login", "Login", new { area = "" });
                }
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new { data = jsSerializer.Serialize(parentRow) });
        }

        [HttpPost]
        public ActionResult UpdateUserProfile(UserManagement u)
        {
            string msg = "";
            try
            {
                u.usm_id = Convert.ToInt32(Session["UserId"]);
                u.usm_updatedby = Convert.ToInt32(Session["UserId"]);
                u.usm_createdby = Session["UserId"].ToString();
                db.AddUserMaster(u);
                msg = "success";
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { status = msg });
        }

        [HttpPost]
        public ActionResult UpdateChangePassword(string OldPasword, string NewPassword)
        {
            string result = "";
            try
            {
                if (Session["UserId"] == null)
                {
                    return RedirectToAction("Login", "Login", new { area = "" });
                }
                result = db.ChangePassword(OldPasword, NewPassword, Session["UserId"].ToString());
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { data = result });
        }
      
        [HttpPost]
        public ActionResult Track_a_job_data()
        {
            int result = 0;
            DataSet ds = db.track_a_cab(Convert.ToInt32(Session["UserId"]));
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            try
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                if (ds.Tables[0].Rows.Count == 0)
                {
                    result = 1;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(new { data = jsSerializer.Serialize(parentRow), result = result });
        }

        [HttpPost]
        public ActionResult UploadProfile(HttpPostedFileBase file)
        {
            string status = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        file = files[i];
                        string fname;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }

                        string FileExt = Path.GetExtension(file.FileName);
                        string F_Fullname = "_" + Session["UserId"].ToString() + FileExt;
                        fname = Path.Combine(Server.MapPath("~/UserPicture/"), F_Fullname);
                        string OldProfile = Session["UserImg"].ToString();
                        string OldFullPath = Server.MapPath("~/UserPicture/" + OldProfile);
                        if ((System.IO.File.Exists(OldFullPath)))
                        {
                            System.IO.File.Delete(OldFullPath);
                        }

                        file.SaveAs(fname);

                        db.UpdateProfile(Session["UserId"].ToString(), F_Fullname);
                        Session["UserImg"] = F_Fullname;
                        status = "success";
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            return Json(new { status = status });
        }

        #endregion  
    }
}