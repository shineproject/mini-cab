﻿
function CheckValueNullOrNOT(str) {
    if (typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g, "") === "") {
        return false;
    }
    else {
        return true;
    }
}

function checkNumber(value) {
    if (value % 1 == 0)
        return false;
    else
        return true;
}

function CheckEmail(val) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(val)) {
        return false;
    } else { return true; }
    //var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    //if (reg.test(val)) {
    //    return true;
    //}
    //else {
    //    return false;
    //}
    //if (!val.match(/\S+@\S+\.\S+/)) { // Jaymon's / Squirtle's solution
    //    // Do something
    //    return false;
    //}
    //if (val.indexOf(' ') != -1 || val.indexOf('..') != -1) {
    //    // Do something
    //    return false;
    //}
    //return true;
}

function deffHoursMinitTwoTime(start_time, end_time) {

    //var start_time = $("#startTime").val();
    //var end_time = $("#endTime").val();
    var startHour = new Date("01/01/2007 " + TwelHoursTo24(start_time)).getHours();
    var endHour = new Date("01/01/2007 " + TwelHoursTo24(end_time)).getHours();
    var startMins = new Date("01/01/2007 " + TwelHoursTo24(start_time)).getMinutes();
    var endMins = new Date("01/01/2007 " + TwelHoursTo24(end_time)).getMinutes();
    var startSecs = new Date("01/01/2007 " + TwelHoursTo24(start_time)).getSeconds();
    var endSecs = new Date("01/01/2007 " + TwelHoursTo24(end_time)).getSeconds();
    var secDiff = endSecs - startSecs;
    var minDiff = endMins - startMins;
    var hrDiff = endHour - startHour;
    return (hrDiff + ":" + minDiff + ":Minutes");

}
function TwelHoursTo24(t) {
    var time = t;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return (sHours + ":" + sMinutes);
}
function CurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '/' + mm + '/' + dd;
    return today;
}
function CurrentDate_dd_mm_yyyy() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = dd + '/' + mm + '/' + yyyy;
    return today;
}

function setformate(str) {
    var date = new Date();
    //var format = "DD-MM-YYYY";
    return dateConvert(date);

    function dateConvert(dateobj, format) {
        var year = dateobj.getFullYear();
        var month = ("0" + (dateobj.getMonth() + 1)).slice(-2);
        var date = ("0" + dateobj.getDate()).slice(-2);

        return (date + '/' + month + '/' + year)
    }
}
function setformate_yyyy_mm_dd(str) {
    //var date = new Date();
    //var format = "DD-MM-YYYY";

    var newdate = str.split("/").reverse().join("-");

    return (newdate);
    // return dateConvert(str);

    function dateConvert(dateobj, format) {
        var year = dateobj.getFullYear();
        var month = ("0" + (dateobj.getMonth() + 1)).slice(-2);
        var date = ("0" + dateobj.getDate()).slice(-2);

        return (year + '/' + month + '/' + date)
    }
}
function GetTime24Format() {
    var d = new Date();
    var h = d.getHours();
    var m = d.getMinutes();
    return h + ":" + m;
}
function MobileValidation(mobile) {
    var phone = mobile.replace(/[^0-9]/g, '');
    if (phone.length < 10 || phone.length >= 15) {
        return false;
    } else {
        return true;
    }
}
function PasswordValidation(inputtxt, minlength, maxlength) {
    var mnlen = minlength;
    var mxlen = maxlength;

    if (inputtxt.length < mnlen || inputtxt.length > mxlen) {
        return false;
    }
    else {
        return true;
    }
}
function UserManuActive(Url) {
    if (Url.match('User_Dashboard')) {
        $('#menu_dashboard').addClass('active');
        $('#menu_book').removeClass('active');
        $('#menu_track').removeClass('active');
        $('#menu_logout').removeClass('active');
    }
    if (Url.match('Book_a_Job')) {
        $('#menu_dashboard').removeClass('active');
        $('#menu_book').addClass('active');
        $('#menu_track').removeClass('active');
        $('#menu_logout').removeClass('active');
    }
    if (Url.match('Track_a_Job')) {
        $('#menu_dashboard').removeClass('active');
        $('#menu_book').removeClass('active');
        $('#menu_track').addClass('active');
        $('#menu_logout').removeClass('active');
    }
}
function profilePicture(Url) {
   
    var status = 0; 
    if (Url.match('User_Dashboard')) {
        status = 0;
    }
    if (Url.match('Book_a_Job')) {
        status = 1;
    }
    if (Url.match('Track_a_Job')) {
        status = 1;
    }
    return status;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#cmp_logo_img')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function formatDate(input) {
    var datePart = input.match(/\d+/g),
    year = datePart[0], // get only two digits
    month = datePart[1], day = datePart[2];

    return day + '/' + month + '/' + year;
}
function jsonDateConvert(date1)
{
    var month = date1.getMonth() + 1
    var day = date1.getDate()
    var year = date1.getFullYear()
    var date = day + "/" + month + "/" + year
    alert(date);
    return date;
}

//update dateTime Use MasterPage

function updateClock() {
    var currentTime = new Date();

    var currentHours = currentTime.getHours();
    var currentMinutes = currentTime.getMinutes();
    var currentSeconds = currentTime.getSeconds();

    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = (currentHours < 12) ? "AM" : "PM";

    // Convert the hours component to 12-hour format if needed
    currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

    // Convert an hours component of "0" to "12"
    currentHours = (currentHours == 0) ? 12 : currentHours;

    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

    // Update the time display
   return currentTimeString;
}
function formatDate_dd_mm_yyy(input) {
    var datePart = input.match(/\d+/g),
    year = datePart[0].substring(0), // get only two digits
    month = datePart[1], day = datePart[2];

    return day + '/' + month + '/' + year;
}

function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode >= 65 && charCode <= 120) && (charCode != 31 && charCode != 0))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
