﻿
(function ($) {
    return $.widget('uiExtension.rangeSlider', $.ui.slider, {
        _create: function () {
            var self;
            self = this;
            self.options.values = [];
            self.options.rangeValues = [];
            if (self.options.overlap == null) {
                self.options.overlap = true;
            }
            $(self.options.ranges).each(function (k, v) {
                v = self._parseRangeValues(v);
                if (v.startValue === v.endValue) {
                    v.endValue = v.startValue + 1;
                }
                self.options.values.push(v.startValue);
                self.options.rangeValues.push(v);
                $(self.element).append('<div class="ui-slider-range ui-widget-header range-bg-' + v.id.toString() + '" style="left: ' + (100 * v.startValue / self.options.max).toString() + '%; width: ' + 100 * (v.endValue - v.startValue) / self.options.max + '%; background: none repeat scroll 0% 0% ' + v.color + ';" ><div class="range-label" style="display:none;"></div></div>');
            });
            return self._super();
        },
        _parseRangeValues: function (obj) {
            var parsed;
            parsed = $.extend(true, {
                id: (new Date).getTime(),
                startValue: 0,
                endValue: 1,
                color: this.options.rangeBarColor
            }, obj);
            return parsed;
        },
        _createHandles: function () {
            var self;
            self = this;
            self._super();
            return this.handles.each(function (index, handler) {
                var range;
                handler = $(handler);
                range = self.options.rangeValues[index];
               
                handler.addClass('range-handler-' + range.id);
                handler.data('id', range.id);
                if (range.status == 1)
                {
                    handler.addClass('white');
                    $(handler).append('<a onclick="assign_job_summary(' + range.from_job_id + ')" style="cursor:pointer;color:transparent;">..</a>');
                } else if (range.status == 2)
                {
                    handler.addClass('gray');
                    $(handler).append('<a onclick="assign_job_summary(' + range.from_job_id + ')" style="cursor:pointer;color:transparent;">..</a>');
                } else if (range.status == 3)
                {
                    handler.addClass('green');
                    $(handler).append('<a onclick="assign_job_summary(' + range.from_job_id + ')" style="cursor:pointer;color:transparent;">..</a>');
                } else if (range.status == 4)
                {
                    handler.addClass('red');
                    $(handler).append('<a onclick="assign_job_summary(' + range.from_job_id + ')" style="cursor:pointer;color:transparent;">..</a>');
                } 
            });
        },
        _checkLimit: function (index, changeValue) {
            var closestLimit, handlers, leftLimit, result, rightLimit, self;
            if (changeValue == null) {
                changeValue = false;
            }
            self = this;
            result = true;
            handlers = self._getHandlers(index);
            closestLimit = 0;
            if ((self.options.overlap == null) || self.options.overlap === false) {
            }
        },
        _refreshInfoBars: function () {
        },
        _refresh: function () {
            this._super();
            return this._refreshInfoBars();
        },
        _rageUiHash: function (event, index) {
            var range, uiHash;
            uiHash = {};
            uiHash.handlers = this._getHandlers(index);
            uiHash.ranges = this.options.ranges;
            range = $.grep(this.options.ranges, function (value) {
                return value.id.toString() === uiHash.handlers.currentHandler.data('id').toString();
            });
            return uiHash;
        },
        _slide: function (event, index, newVal) {
            var allow, handlers, self, ui, width;
            self = this;
            allow = false;
            handlers = self._getHandlers(index);
            handlers.currentHandler.data('value', self.options.values[index]);
            ui = self._rageUiHash(event, index);
            if (self._checkLimit(index) === false) {
                allow = false;
            }
            if (allow === false) {
                return false;
            } else {
                allow = self._trigger('rangeSlide', event, ui);
            }
            self._renderInfoBar(event, index);
            allow = this._super(event, index, newVal);
            return allow;
        },
        _stop: function (event, index) {
            this._checkLimit(index, true);
            return this._super(event, index);
        },
        _renderInfoBar: function (event, index) {
            var self, text, ui;
            self = this;
            text = "";
            ui = self._rageUiHash(event, index);
            ui.label = $('.range-label', ui.handlers.rangeBar);
            if ($.isFunction(self.options.rangeLabel)) {
                return self._trigger('rangeLabel', event, ui);
            } else {
                return ui.label.empty().append(text);
            }
        },
        _getHandlers: function (index) {
            var handlerSelector, handlers, self;
            handlers = {};
            self = this;
            handlers.currentHandler = $(self.handles[index]);
            handlerSelector = '.range-handler-' + handlers.currentHandler.data('id');
            handlers.leftHandler = $(handlerSelector + '.left:first', self.element);
            handlers.rightHandler = $(handlerSelector + '.right:first', self.element);
            handlers.rangeBar = $('.range-bg-' + handlers.currentHandler.data('id'), self.element);
            return handlers;
        }
    });
})($);
