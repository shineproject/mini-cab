

var myarr = [];
var myarrNew = [];
var mainDriverDB = [];

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
if (dd < 10) {
    dd = '0' + dd;
}
if (mm < 10) {
    mm = '0' + mm;
}
var today = yyyy + '/' + mm + '/' + dd;
function unique(list) {
    var result = [];
    $.each(list, function (i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}
function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}
var DriverList = [];
var TimeValueList = [];
$.ajax({
    type: "POST",
    dataType: "json",
    url: '../../Admin/OperatorConsole/GetAssignJobsDateWise',
    data: { selectDateAssign: today },
    success: function (data) {
        var count = 1
        var dataJSON = JSON.parse(data.data);

        _.each(dataJSON, function (item) {
            DriverList.push(item.Driver_Id);
        });
        _.each(unique(DriverList), function (DriverNO) {
            var Driver_DriverNO = [];
            _.each(dataJSON, function (item) {
                if (item.Driver_Id == DriverNO) {
                    Driver_DriverNO.push(item);
                }
            });
            mainDriverDB.push(Driver_DriverNO);
        });
        //create div driver wise
        _.each(unique(DriverList), function (DriverNOId) {
            $('#MainDiv').append('<div class="box-border" style="height:44px;">' +
                                       '<div class="green-border" id="d_Login_' + DriverNOId + '"></div>' +
                                       '<div class="second-box">' +
                                           '<ul><li>route d <span class="first-no" id="R_id_' + DriverNOId + '"></span></li>' +
                                               '<li>driver d <span class="first-no2"id="D_id_' + DriverNOId + '"></span></li>' +
                                               '<li>[ET A] to [Next Waypdrt] <span></span></li></ul>' +
                                      ' </div>' +
                                       '<div class="last-box">' +
                                           '<div class="first-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="third-box"></div>' +
                                           '<div class="second-box2"></div>' +
                                           '<div class="first-box"></div>' +
                                           '<div>' +
                                               '<div class="border-line border-line-00"></div>' +
                                               '<div class="border-line border-line-01"></div>' +
                                               '<div class="border-line border-line-02"></div>' +
                                               '<div class="border-line border-line-03"></div>' +
                                               '<div class="border-line border-line-04"></div>' +
                                               '<div class="border-line border-line-05"></div>' +
                                               '<div class="border-line border-line-06"></div>' +
                                               '<div class="border-line border-line-07"></div>' +
                                               '<div class="border-line border-line-08"></div>' +
                                               '<div class="border-line border-line-09"></div>' +
                                               '<div class="border-line border-line-10"></div>' +
                                               '<div class="border-line border-line-11"></div>' +
                                               '<div class="border-line border-line-12"></div>' +
                                               '<div class="border-line border-line-13"></div>' +
                                               '<div class="border-line border-line-14"></div>' +
                                               '<div class="border-line border-line-15"></div>' +
                                               '<div class="border-line border-line-16"></div>' +
                                               '<div class="border-line border-line-17"></div>' +
                                               '<div class="border-line border-line-18"></div>' +
                                               '<div class="border-line border-line-19"></div>' +
                                               '<div class="border-line border-line-20"></div>' +
                                               '<div class="border-line border-line-21"></div>' +
                                               '<div class="border-line border-line-22"></div>' +
                                               '<div class="border-line border-line-23"></div>' +
                                               '<div class="border-line border-line-24"></div>' +
                                           '</div>' +
                                       '</div>' + '<div id="slider-range-' + DriverNOId + '" style="margin-bottom: 8%;width: 75%;left: 24%;margin: 0px !important;top: 14px;"></div>' +
                                   '</div>');
            // $('.demo').append('<div id="slider-range-' + DriverNOId + '" style="margin-bottom: 8%;"></div>')

        });

        for (index = 0, len = mainDriverDB.length; index < len; ++index) {
            TimeValueList = [];
            _.each(mainDriverDB[index], function (Time_item) {
                TimeValueList.push(parseInt(Time_item.Ctime.replace(':', '')));
            });
            myarr = [];
            _.each(mainDriverDB[index], function (itemSource) {
                var bgcolor = '';
               
                hrs = itemSource.Ctime.replace(':', '');
                if (itemSource.Noc_Status == 1) { bgcolor = 'white'; }
                else if (itemSource.Noc_Status == 2) { bgcolor = 'gray'; }
                else if (itemSource.Noc_Status == 3) { bgcolor = 'green'; }
                else if (itemSource.Noc_Status == 4) { bgcolor = 'red'; }

                myarr.push({ "id": count, "startValue": hrs, "endValue": getMaxOfArray(TimeValueList), "color": bgcolor, "data": itemSource.Assign_Time, "status": itemSource.Noc_Status, "Driver_Id": itemSource.Driver_Id, "RouteId": itemSource.RouteId, "LoginStatus": itemSource.LoginStatus });
                count++;
            });
            MakeRangeSlider(myarr[0].Driver_Id);

            $('#D_id_' + myarr[0].Driver_Id).text(myarr[0].Driver_Id);
            $('#R_id_' + myarr[0].Driver_Id).text(myarr[0].RouteId);
            if (myarr[0].LoginStatus == 0) {
                $('#d_Login_' + myarr[0].Driver_Id).addClass('red-border');
            }
        }
       
    }
});

function MakeRangeSlider(divId) {

    var renderLabel;
    renderLabel = function (ui, customContent) {
        var content, endAt, range, startAt;
        if (customContent == null) {
            customContent = false;
        }
        range = ui.range;
        startAt = moment(range.startAt, "YYYY-MM-DD h:mm").add(range.startValue, "minutes");
       
        return content;
    };
    $('#slider-range-' + divId).rangeSlider({
        min: 0,
        max: 2399,
        ranges: myarr
    });
};
