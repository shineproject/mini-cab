﻿using API.Common_Function;
using API.Models;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class NoCollectionController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // POST: api/NoCollection
        public IHttpActionResult Post(NoCollection NOC_obj)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                db.Add_NoCollection(NOC_obj);
                response.Message = "success";

                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

    }
}
