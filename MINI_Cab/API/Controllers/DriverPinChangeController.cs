﻿using API.Common_Function;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class DriverPinChangeController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/DriverPinChange
        [HttpPost]
        public IHttpActionResult Pin(string DriverId,string OldPin,string NewPin)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                string result = db.ChangePassword(DriverId.Trim(), OldPin.Trim(), NewPin.Trim());
                response.Message = result;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/DriverPinChange
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/DriverPinChange/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/DriverPinChange/5
        //public void Delete(int id)
        //{
        //}
    }
}
