﻿using API.Common_Function;
using API.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using System.Data;
using System.Web.Script.Serialization;
using Elmah;

namespace API.Controllers
{
    public class RegistrationController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();

        [HttpPost]
        public IHttpActionResult Registation(User_mgt Reg)
        {
            int userId = 0;
            JsonResponse response = new JsonResponse();
            try
            {
                Reg.usm_userrole = 2;
                userId = db.AddUserMaster(Reg);
                DataSet ds = db.GetUserData(userId);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                response.Message = "success";
                response.data = parentRow;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }

        //[HttpPost]
        //public IHttpActionResult Login(User_Login l)
        //{
        //    JsonResponse response = new JsonResponse();
        //    try
        //    {
        //        DataSet ds =db.CheckLogin(l.Email, l.Password);
        //        response.Message = "success";
        //        response.data = ds;
        //        response.HttpStatusCode = (int)HttpStatusCode.OK;
        //        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        //    }
        //    catch (Exception)
        //    {
        //        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
        //        response.Message = "Error Occured.";
        //        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
        //    }

        //}
    }
}
