﻿using API.Common_Function;
using API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using API.Models;
using Elmah;

namespace API.Controllers
{
    public class UserLoginController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/UserLogin
        [HttpPost]
        public IHttpActionResult Login(Login l)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = db.CheckUserLogin(l.Email.Trim(), l.Password.Trim());
                if (ds.Tables[0].Rows.Count == 1)
                {
                    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                    Dictionary<string, object> childRow;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        childRow = new Dictionary<string, object>();
                        foreach (DataColumn col in ds.Tables[0].Columns)
                        {
                            childRow.Add(col.ColumnName, row[col]);
                        }
                        parentRow.Add(childRow);
                    }
                    response.Message = "success";
                    response.data = parentRow;
                    response.HttpStatusCode = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Message = "fail";
                }
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
