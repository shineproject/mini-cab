﻿using API.Common_Function;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace API.Controllers
{
    public class DriverLoginController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/DriverLogin
        public IHttpActionResult Get(int DriverId)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                db.Logout(DriverId, 2);
                response.Message = "success";

                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        // POST: api/DriverLogin
        [HttpPost]
        public async Task<IHttpActionResult> Post(string Email, string Password)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                //Thread.Sleep(2000);
                string result = db.CheckLogin_Logout(Email.Trim(), Password.Trim(), "3");
                if (result == "0")
                {
                    DataSet ds = await db.CheckDriverLogin(Email.Trim(), Password.Trim(), "1");
                    //Thread.Sleep(2000);
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                        Dictionary<string, object> childRow;
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            childRow = new Dictionary<string, object>();
                            foreach (DataColumn col in ds.Tables[0].Columns)
                            {
                                childRow.Add(col.ColumnName, row[col]);
                            }
                            parentRow.Add(childRow);
                        }
                        response.Message = "success";
                        response.data = parentRow;
                        response.HttpStatusCode = (int)HttpStatusCode.OK;
                    }
                    else
                    {
                        response.Message = "fail";
                    }
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                }
                else if (result == "1")
                {
                    response.Message = "Already Login Driver";
                    response.HttpStatusCode = (int)HttpStatusCode.OK;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                }
                else
                {
                    response.Message = "fail login";
                    response.HttpStatusCode = (int)HttpStatusCode.OK;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
