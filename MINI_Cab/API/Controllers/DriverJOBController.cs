﻿using API.Common_Function;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace API.Controllers
{
    public class DriverJOBController : ApiController
    {
        // GET: api/DriverJOB
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/DriverJOB/5
        [HttpGet]
        public IHttpActionResult Get(int DriverId)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = db.DriverJob(DriverId);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    response.Message = "success";
                    response.HttpStatusCode = (int)HttpStatusCode.OK;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        childRow = new Dictionary<string, object>();
                        foreach (DataColumn col in ds.Tables[0].Columns)
                        {
                            childRow.Add(col.ColumnName, row[col]);
                        }
                        parentRow.Add(childRow);
                    }
                    response.data = parentRow;
                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "no data found";
                }
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));

            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = ex.ToString();
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }

        // POST: api/DriverJOB
        //public void Post([FromBody]string value)
        //{
        //}
        [HttpGet]
        public IHttpActionResult ComplateJOB(int JOBId, int DriverId, int Process)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                //NoCollection c = new NoCollection();
                db.ComplateJOB(JOBId, DriverId, Process);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Success";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

    }
}
