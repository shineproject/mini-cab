﻿using API.Common_Function;
using API.Models;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace API.Controllers
{
    public class DriverNotificationController : ApiController
    {
        // GET: api/DriverNotification
        public IHttpActionResult Get(int DriverId)
        {
            Common_FunctionAPI db = new Common_FunctionAPI();
            JsonResponse response = new JsonResponse();
            try
            {
                string result = db.DriverNotification(DriverId);
                response.Message = result;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        [HttpPost]
        public IHttpActionResult DriverSummary(DriverSummary DSObj)
        {
            Common_FunctionAPI db = new Common_FunctionAPI();
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet result = new DataSet();
                if (DSObj.Type.ToLower() == "today")
                {
                    DateTime StartDate, EndDate;
                    StartDate = DateTime.Now;
                    EndDate = DateTime.Now;
                    var StartConvert = StartDate.ToString("yyyy/MM/dd");
                    var EndConvert = EndDate.ToString("yyyy/MM/dd");
                    result = db.DriverSummary(DSObj.DriverId, StartConvert, EndConvert);
                }
                else if(DSObj.Type.ToLower() == "week")
                {
                    DateTime StartDate, EndDate;
                    StartDate = DateTime.Now;
                    EndDate = DateTime.Now.AddDays(-7);
                    var StartConvert = StartDate.ToString("yyyy/MM/dd");
                    var EndConvert = EndDate.ToString("yyyy/MM/dd");
                    result = db.DriverSummary(DSObj.DriverId, EndConvert, StartConvert);
                }
                else if (DSObj.Type.ToLower() == "month")
                {
                    DateTime StartDate, EndDate;
                    StartDate = DateTime.Now;
                    EndDate = DateTime.Now.AddMonths(-1);
                    var StartConvert = StartDate.ToString("yyyy/MM/dd");
                    var EndConvert = EndDate.ToString("yyyy/MM/dd");
                    result = db.DriverSummary(DSObj.DriverId, EndConvert, StartConvert);
                }
                else if (DSObj.Type.ToLower() == "year")
                {
                    DateTime StartDate, EndDate;
                    StartDate = DateTime.Now;
                    EndDate = DateTime.Now.AddYears(-1);
                    var StartConvert = StartDate.ToString("yyyy/MM/dd");
                    var EndConvert = EndDate.ToString("yyyy/MM/dd");
                    result = db.DriverSummary(DSObj.DriverId, EndConvert, StartConvert);
                }
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in result.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in result.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                response.Message = "Sucess";
                response.data = parentRow;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

    }
}
