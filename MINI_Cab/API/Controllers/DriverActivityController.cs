﻿using API.Common_Function;
using API.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using Elmah;

namespace API.Controllers
{
    public class DriverActivityController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/DriverActivity
        [HttpPost]
        public IHttpActionResult Activity(DriverActivity obj)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                db.ADD_ActivityDriver(obj);
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
