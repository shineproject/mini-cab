﻿using API.Common_Function;
using API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using API.Models;
using Elmah;

namespace API.Controllers
{
    public class Book_JourneyController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/Book_Journey/5
        [HttpPost]
        public IHttpActionResult Journey(Journey obj)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                int from_jobid = db.Add_from_job(obj);

                string[] to_job = obj.tojob_data.TrimEnd('+').Split('+');
                foreach (var job in to_job)
                {
                    string[] single_job = job.Split('$');
                    db.Add_to_job(single_job[2], single_job[1], single_job[0], from_jobid);
                }
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }

        [HttpGet]
        public IHttpActionResult GetTariff()
        {
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = db.get_tariffs(0);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                response.Message = "success";
                response.data = parentRow;
                response.HttpStatusCode = (int)HttpStatusCode.OK;

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
