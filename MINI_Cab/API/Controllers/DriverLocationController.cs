﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using API.Utility;
using API.Common_Function;
using Elmah;

namespace API.Controllers
{
    public class DriverLocationController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/DriverLocation
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/DriverLocation/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/DriverLocation
        [HttpPost]
        public IHttpActionResult Post(DriverLocation driver)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                db.DriverLocation(driver);
                response.Message = "success";

                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        // PUT: api/DriverLocation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DriverLocation/5
        public void Delete(int id)
        {
        }
    }
}
