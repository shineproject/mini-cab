﻿using API.Common_Function;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace API.Controllers
{
    public class Track_JourneyController : ApiController
    {
        // GET: api/Track_Journey
        Common_FunctionAPI db = new Common_FunctionAPI();
        [HttpGet]
        public IHttpActionResult Track_Journey(int Userid)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = db.track_a_cab(Userid);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                response.Message = "success";
                response.data = parentRow;
                response.HttpStatusCode = (int)HttpStatusCode.OK;

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }

        [HttpPost]
        public IHttpActionResult LastFiveJourney(int Userid)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = db.LastFiveJourney(Userid);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                response.Message = "success";
                response.data = parentRow;
                response.HttpStatusCode = (int)HttpStatusCode.OK;

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
