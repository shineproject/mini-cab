﻿using API.Common_Function;
using API.Utility;
using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class ActualAmountController : ApiController
    {
        Common_FunctionAPI db = new Common_FunctionAPI();
        // GET: api/ActualAmount
        [HttpPost]
        public IHttpActionResult ActualAmount_Submit(int JOBId, decimal Amount, decimal Miles)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                string result = db.Actual_ammount(JOBId, Amount, Miles);
                if (result == "success")
                {
                    response.Message = "success";
                }
                else {
                    response.Message = "Fail";
                }
               
                response.HttpStatusCode = (int)HttpStatusCode.OK;

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }
    }
}
