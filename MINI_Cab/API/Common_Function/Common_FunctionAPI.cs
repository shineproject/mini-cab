﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using API.Models;
using System.Data;
using System.Threading.Tasks;

namespace API.Common_Function
{
    public class Common_FunctionAPI
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString);
        #region Opration Area
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int AddUserMaster(User_mgt reg)
        {
            try
            {
                int Action = 0;
                int UserMasterid = 0;
                SqlCommand command;
                ConnectionCheck();
                if (reg.usm_id == 0)
                {
                    command = new SqlCommand("Add_UserManage_SP", con);
                    Action = 1;
                }
                else
                {
                    command = new SqlCommand("Add_UserManage_SP", con);
                    command.Parameters.AddWithValue("@usm_id", reg.usm_id);
                    Action = 2;
                }

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@usm_fname", reg.usm_fname);
                command.Parameters.AddWithValue("@usm_lname", reg.usm_lname);
                command.Parameters.AddWithValue("@usm_password", reg.usm_password);
                command.Parameters.AddWithValue("@usm_email", reg.usm_email);
                command.Parameters.AddWithValue("@usm_phone", reg.usm_phone);
                command.Parameters.AddWithValue("@usm_address", reg.usm_address);
                command.Parameters.AddWithValue("@usm_city", reg.usm_city);
                command.Parameters.AddWithValue("@usm_state", reg.usm_state);
                command.Parameters.AddWithValue("@usm_country", reg.usm_country);
                command.Parameters.AddWithValue("@usm_userrole", reg.usm_userrole);
                command.Parameters.AddWithValue("@usm_com_id", 0);
                command.Parameters.AddWithValue("@usm_brn_id", 0);
                command.Parameters.AddWithValue("@usm_status", "0");
                command.Parameters.AddWithValue("@usm_createddate", DateTime.Now);
                command.Parameters.AddWithValue("@usm_createdby", 0);
                command.Parameters.AddWithValue("@usm_updateddate", DateTime.Now);
                command.Parameters.AddWithValue("@usm_updatedby", 0);
                command.Parameters.AddWithValue("@Action", Action);

                if (Action == 2)
                {
                    command.ExecuteNonQuery();
                    UserMasterid = reg.usm_id;
                }
                else
                {
                    UserMasterid = (int)command.ExecuteScalar();
                }
                con.Close();
                return UserMasterid;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public DataSet GetUserData(int id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetUserProfile_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@userId", id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string CheckLogin_Logout(string Email, string Password, string Action)
        {
            var result  = "3";
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("CheckLoginDriver_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Email", Email);
                command.Parameters.AddWithValue("@Password", Password);
                command.Parameters.AddWithValue("@Action", Action);
                SqlDataReader RDR = command.ExecuteReader();
                if (RDR.Read())
                {
                    result = RDR["driver_status"].ToString();
                }
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public DataSet CheckLogin(string usm_fname, string usm_password)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("CheckLogin_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_email", usm_fname);
                command.Parameters.AddWithValue("@usm_password", usm_password);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public Task<DataSet> CheckDriverLogin(string Email, string Password, string Action)
        {
            DataSet ds = new DataSet();
            try
            {
                ConnectionCheck();
                SqlCommand command = new SqlCommand("CheckLoginDriver_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Email", Email);
                command.Parameters.AddWithValue("@Password", Password);
                command.Parameters.AddWithValue("@Action", Action);
                return Task.Run(() =>
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        sda.Fill(ds);
                    }
                    ds.Tables[0].TableName = "Data";
                    con.Close();
                    return ds;
                }
            );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        public void Logout(int LoginId, int Action_US)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command1 = new SqlCommand("CheckLoginDriver_SP", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@Email", LoginId);
                command1.Parameters.AddWithValue("@Password", "bb");
                command1.Parameters.AddWithValue("@Action", Action_US);
                command1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet DriverJob(int DriverId)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("DriverByJOB_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Driver_Id", DriverId);
                command.Parameters.AddWithValue("@date", DateTime.Now);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public void DriverLocation(DriverLocation Driver)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("Update_DriverLocation_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DriverId", Driver.DriverId);
                command.Parameters.AddWithValue("@Let", Driver.let);
                command.Parameters.AddWithValue("@Lng", Driver.lng);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public DataSet CheckUserLogin(string usm_fname, string usm_password)
        {
            try
            {
                DataSet ds = new DataSet();
                ConnectionCheck();
                SqlCommand command = new SqlCommand("CheckLogin_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usm_email", usm_fname);
                command.Parameters.AddWithValue("@usm_password", usm_password);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public void ComplateJOB(int JOBId, int DriverId, int Process)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command = new SqlCommand("ComplateJOB_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@JOBId", JOBId);
                command.Parameters.AddWithValue("@DriverId", DriverId);
                command.Parameters.AddWithValue("@Action", Process);
                command.ExecuteNonQuery();
                con.Close();
              
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Actual_ammount(int JOBId, decimal Amount,decimal Miles)
        {
            var result = "fail";
            try
            {
                ConnectionCheck();
                SqlCommand command = new SqlCommand("ActualAmmount_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@jobid", JOBId);
                command.Parameters.AddWithValue("@Amount", Amount);
                command.Parameters.AddWithValue("@Miles", Miles);
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected == 1)
                {
                    result = "success";
                }
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public DataSet get_tariffs(int tariff_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("get_tariff_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tariff_id", tariff_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public int Add_from_job(Journey f)
        {
            try
            {
                SqlCommand command1;
                ConnectionCheck();
                command1 = new SqlCommand("add_fromjob_sp", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@from_job_name", f.from_job_name);
                command1.Parameters.AddWithValue("@from_job_number", f.from_job_number);
                command1.Parameters.AddWithValue("@from_job_street", f.from_job_street);
                command1.Parameters.AddWithValue("@from_job_postcode", f.from_job_postcode);
                command1.Parameters.AddWithValue("@from_job_address", f.from_job_address);
                command1.Parameters.AddWithValue("@from_job_notes", f.from_job_notes);
                command1.Parameters.AddWithValue("@from_job_time", f.from_job_time);
                command1.Parameters.AddWithValue("@VehicleType", f.tariff_id);
                command1.Parameters.AddWithValue("@status", f.status);
                command1.Parameters.AddWithValue("@user_id", f.user_id);
                command1.Parameters.AddWithValue("@Pay_data", f.PaymentData);
                command1.Parameters.AddWithValue("@Pay_TypeId", f.PaymentType);
                command1.Parameters.AddWithValue("@Amount", f.Amount);
                command1.Parameters.AddWithValue("@Miles", f.Miles);
                command1.Parameters.AddWithValue("@PickupTime", DateTime.Now);
                command1.Parameters.AddWithValue("@DropTime", DateTime.Now);
                command1.Parameters.AddWithValue("@WayType", f.WayType);
                int from_jobid = (int)command1.ExecuteScalar();
                con.Close();
                return from_jobid;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void Add_to_job(string tojob_street, string tojob_postcode, string tojob_address, int from_jobid)
        {
            try
            {
                SqlCommand command1;
                ConnectionCheck();
                command1 = new SqlCommand("add_tojob_SP", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@tojob_street", tojob_street);
                command1.Parameters.AddWithValue("@tojob_postcode", tojob_postcode);
                command1.Parameters.AddWithValue("@tojob_address", tojob_address);
                command1.Parameters.AddWithValue("@from_job_id", from_jobid);
                command1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet track_a_cab(int user_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("track_a_cab_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@user_id", user_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public DataSet LastFiveJourney(int user_id)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("LastFiveJobs_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@User_ID", user_id);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                ds.Tables[0].TableName = "Data";
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public void ADD_ActivityDriver(DriverActivity d)
        {
            try
            {
                ConnectionCheck();
                SqlCommand command;
                command = new SqlCommand("Activity_Add_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Activity_Name", d.Activity_Name);
                command.Parameters.AddWithValue("@Activity_Time", d.Activity_Time);
                command.Parameters.AddWithValue("@Activity_User", d.Activity_User);
                command.Parameters.AddWithValue("@Activity_Job", d.Activity_Job);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public string ChangePassword(string DriverId, string OldPin, string NewPing)
        {
            string status_msg = "";
            SqlCommand command;
            ConnectionCheck();

            command = new SqlCommand("ChangePin_SP", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@driver_id", DriverId);
            command.Parameters.AddWithValue("@Old_Password", OldPin);
            command.Parameters.AddWithValue("@New_Password", NewPing);
            command.Parameters.AddWithValue("@Action", "Check");
            int result = (int)command.ExecuteScalar();
            if (result != 0)
            {
                status_msg = Confirm_ChangePassword(DriverId, OldPin, NewPing);
            }
            else { status_msg = "OLD Password Not Match"; }
            return status_msg;

        }
        public string Confirm_ChangePassword(string DriverId, string OldPin, string NewPing)
        {
            SqlCommand command;
            ConnectionCheck();
            command = new SqlCommand("ChangePin_SP", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@driver_id", DriverId);
            command.Parameters.AddWithValue("@Old_Password", OldPin);
            command.Parameters.AddWithValue("@New_Password", NewPing);
            command.Parameters.AddWithValue("@Action", "UpdatePassword");
            command.ExecuteNonQuery();
            return "Change Password Successfully";
        }
        public string DriverNotification(int DriverId)
        {
            string result = "0";
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetNotificationByDriverId_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DriverId", DriverId);
                SqlDataReader RDR = command.ExecuteReader();
                if (RDR.Read())
                {
                    result = RDR["from_job_id"].ToString();
                }
                
                con.Close();
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public DataSet DriverSummary(string DriverId,string StartDate,string EndDate)
        {
            try
            {
                ConnectionCheck();
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("DriverSummery_SP", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DriverId", DriverId);
                command.Parameters.AddWithValue("@StartDate", StartDate);
                command.Parameters.AddWithValue("@EndDate", EndDate);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void Add_NoCollection(NoCollection obj_NOC)
        {
            try
            {
                SqlCommand command1;
                ConnectionCheck();
                command1 = new SqlCommand("Add_NoCollection", con);
                command1.CommandType = CommandType.StoredProcedure;
                command1.Parameters.AddWithValue("@Noc_Driver_Id", obj_NOC.Noc_Driver_Id);
                command1.Parameters.AddWithValue("@Noc_JobId", obj_NOC.Noc_JobId);
                command1.Parameters.AddWithValue("@Noc_Type", obj_NOC.Noc_Type);
                command1.Parameters.AddWithValue("@Noc_ExpTime", obj_NOC.Noc_ExpTime);
                command1.Parameters.AddWithValue("@Noc_Reason", obj_NOC.Noc_Reason);
                command1.Parameters.AddWithValue("@Noc_CreatedBy", obj_NOC.Noc_Driver_Id);
                command1.Parameters.AddWithValue("@Noc_CreatedDate", DateTime.Now);
                command1.Parameters.AddWithValue("@Noc_UpdatedBy", obj_NOC.Noc_Driver_Id);
                command1.Parameters.AddWithValue("@Noc_UpdatedDate", DateTime.Now);
                command1.Parameters.AddWithValue("@Action", "INSERT");
                command1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion
    }
}