﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class NoCollection
    {
        public int Noc_Id { get; set; }
        public int Noc_Driver_Id { get; set; }
        public int Noc_JobId { get; set; }
        public string Noc_Type { get; set; }
        public DateTime Noc_ExpTime { get; set; }
        public string Noc_Reason { get; set; }

    }
}