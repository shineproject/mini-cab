﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class DriverSummary
    {
        public string DriverId { get; set; }
        public string Type { get; set; }
    }
}