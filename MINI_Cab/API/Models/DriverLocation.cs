﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class DriverLocation
    {
       public int DriverId { get; set; }
       public string let { get; set; }
       public string lng { get; set; }
    }
}