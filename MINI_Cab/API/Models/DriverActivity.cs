﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class DriverActivity
    {
        public string Activity_Name { get; set; }
        public string Activity_Time { get; set; }
        public string Activity_User { get; set; }
        public string Activity_Job { get; set; }
    }
}