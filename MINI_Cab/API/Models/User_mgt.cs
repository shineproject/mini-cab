﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class User_mgt
    {
        public int usm_id { get; set; }
        public string usm_fname { get; set; }
        public string usm_lname { get; set; }
        public string usm_password { get; set; }
        public string usm_email { get; set; }
        public string usm_phone { get; set; }
        public string usm_address { get; set; }
        public string usm_city { get; set; }
        public string usm_state { get; set; }
        public string usm_country { get; set; }
        public int usm_userrole { get; set; }
        public string usm_status { get; set; }
        public string usm_img { get; set; }
    }
    public class User_Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}