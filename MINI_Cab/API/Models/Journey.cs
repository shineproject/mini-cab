﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class Journey
    {
        public int from_job_id { get; set; }
        public string from_job_name { get; set; }
        public string from_job_number { get; set; }
        public string from_job_street { get; set; }
        public string from_job_postcode { get; set; }
        public string from_job_address { get; set; }
        public string from_job_notes { get; set; }
        public DateTime from_job_time { get; set; }
        public int tariff_id { get; set; }
        public int status { get; set; }
        public int user_id { get; set; }
        public string tojob_data { get; set; }
        public string PaymentData { get; set; }
        public Int32 PaymentType { get; set; }
        public decimal Amount { get; set; }
        public decimal Miles { get; set; }
        public DateTime PickupTime { get; set; }
        public DateTime DropTime { get; set; }
        public Boolean WayType { get; set; }
    }
}